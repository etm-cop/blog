Title: Books and Stories
SubTitle: Exercise #3
Tags: User Stories, Agile
Date: 2015-12-09
Author: Kevin Perry
Summary: Define user stories and acceptance criteria for a library management system.

Define an **epic** and **user stories** for a library management system:

- The *team leads* meet with a *customer representative* and define an epic
    based on the customer's wishes.
- Each *team lead* gets together with a *developer* and have them define user
    stories and relevant acceptance criteria.

## Template
Stick to the following templates when writing down the user stories:

### User Story
As a `______` (*role*),
I want `______` (*goal/desire*)
so that `______` (*benefit*).

### Acceptance Criteria
Given **some context**,
when **some action is carried out**,
then **observable consequences occur**.

## INVEST
When defining a user story, keep in mind the *INVEST* mnemomic:

- **I**ndependent: The user story should be self-contained, in a way that there
    is no inherent dependency on another user story.
- **N**egotiable: User stories, up until they are part of an iteration, can
    always be changed and rewritten.
- **V**aluable: A user story must deliver value to the end user.
- **E**stimatable: You must always be able to estimate the size of a user story.
- **S**mall: User stories should not be so big as to become impossible to
    plan/task/prioritize with a certain level of certainty.
- **T**estable: The user story or its related description must provide the
    necessary information to make test development possible.
