Title: Hard to Forget
SubTitle: Exercise #1
Tags: TDD, C++11, unit testing
Date: 2015-10-07
Author: Kevin Perry
Summary: Develop and test a recently-used list class.

Develop a recently-used list class to hold strings uniquely in LIFO order.
Also write unit tests that cover the specified requirements.
A TDD and/or BDD approach is appreciated but not mandatory.

## Requirements
- A recently-used-list is initially empty.
- The most recently added item is first.
- The least recently added item is last.
- Items can be looked up by index, which counts from zero.
- Items in the list are unique, so duplicate insertions are moved rather than
    added.

### Optional
- Null insertions (empty strings) are not allowed.
- A bounded capacity can be specified, so there is an upper limit to the number
    of items contained, with the least recently added items dropped on overflow.

## Unit Test Framework
While it's up to you, the suggested UT framework for this exercise is [Catch](https://github.com/philsquared/Catch).
Catch is "a modern, C++-native, header-only, framework for unit-tests, TDD and BDD".
For you to get started, you can check out the [Tutorial](https://github.com/philsquared/Catch/blob/master/docs/tutorial.md).
