Title: Alarming Development
SubTitle: Exercise #2
Tags: analysis and design, design patterns
Date: 2015-10-28
Author: Kevin Perry
Summary: Object-oriented analysis and design of a simple alarm system

You are responsible for the alarm system of a plant.
In the plant, there are several devices communicating with your system.
They use a very simple interface with the method
`handleAlarm(text: String): Alarm`.

Alarms should be displayed in a list.
Due to space restrictions only 10 alarms are displayed at once.
In order to be displayed, please add the alarms to the `AlarmView` class.
If the `AlarmView` is able to view your message, you will get a `view` event.
If your alarm is hidden again, you receive a `hide` event.
If the alarm is viewed previously, it can be acknowledged on the `AlarmView`
window.
In that case you get an `ack` event.

When the alarms are created, one ore more sirens should receive the message
`startDelayed`.
If the alarm is acknowledged, the siren should receive an `abort` message.

When the `conditionGone` event is received (that could happen any time), the
alarm is set `inactive`.
The siren receives an `abort` message in that case.
In the case the `conditionReappeared` event is received, the alarm should be
treated like a new one (the means, it starts in the `hidden` state again).

Discuss the requirements with the group and draw pertinent UML diagrams.
*Class*, *state*, and *sequence* diagrams are suggested.
