Title: Desperate Measurements
SubTitle: Exercise #5
Tags: hardware, REST, databases, wireless, webservice, project
Date: 2016-06-15
Author: Kevin Perry
Summary: Build a distributed wireless sensor network and visualize acquired data

In this multidisciplinary project your team will plan and implement a distributed sensor network.
Identify discrete components, split up the work and define interfaces between them.
Research and discuss how to best implement your individual components.

## Requirements
- Have at least one dedicated sensor node and visualizer.
- The sensor node should maintain at least one sensor (e.g. a temperature sensor).
- Communication between sensor nodes and the aggregator should be wireless.
- The system should both calculate and persist relevant statistical data.
- A RESTful webservice will provide both live and statistical data.
- A visualizer provides pretty graphical output for both live and statistical data.

## Deployment
Deployment should look roughly like this:

- Multiple **SensorNodes** are placed throughout the building.
- Each **SensorNode** hosts one or more sensor for which data is acquired in a sensible interval.
- The data is distributed (or queried) *wirelessly* to (or by) an **AggregatorNode**.
- The **AggregatorNode** aggregates data from all **SensorNodes** and sends it
    - to a **PersistencyNode** for long-time storage and statistics, and
    - to a **WebServiceNode** that distributes information to subscribed **VisualizerNodes**.
- The **PersistencyNode** stores acquired data (and relevant calculated statistics) and provides it
    to a **WebService**.
- The **WebService** provides a *RESTful* API that allows to both *query* statistical data and
    to *subscribe* for live data.
- The **VisualizerNode** *queries* and *subscribes* for sensor data and displays them.

![Possible Deployment]({filename}/images/exercises/deployment_desperate-measurements.png)
