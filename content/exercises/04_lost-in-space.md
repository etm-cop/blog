Title: Lost in Space
SubTitle: Exercise #4
Tags: algorithms, TDD, C++11, unit testing
Date: 2016-05-04
Author: Kevin Perry
Summary: Develop and test a spatial data structure.

Develop an associative container class to hold arbitrary objects identified by a point in 2-dimensional Euclidean space.
Also write unit tests that cover the specified requirements.
A TDD and/or BDD approach is appreciated but not mandatory.

## Requirements
- A spatial container is initially empty.
- The container needs to support at least 2D Euclidean space.
- N-dimensional points serve as key.
- Items can be looked up by
    - their specific point
    - a bounding box defined by two points
    - a bounding circle defined by its center and radius
    - the N closest to a given point.
- The same key may be used multiple times.
- Optimize towards lookup.

## Unit Test Framework
While it's up to you, the suggested UT framework for this exercise is [Catch](https://github.com/philsquared/Catch).
Catch is "a modern, C++-native, header-only, framework for unit-tests, TDD and BDD".
For you to get started, you can check out the [Tutorial](https://github.com/philsquared/Catch/blob/master/docs/tutorial.md).
