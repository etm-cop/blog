Title: Patterns of Interest
SubTitle: Recurring Exercise
Tags: design patterns, mentoring, presentations
Author: Kevin Perry
Date: 2016-06-15
Summary: A shortlist of patterns for upcoming mentoring sessions.

- **Design Patterns - Elements of Reusable Object-Oriented Software**
    - Abstract Factory, Factory Method
    - Decorator
    - *Flyweight*
    - [Command]({filename}command.md)
    - Mediator
    - *Observer (Signal/Slot Mechanism)*
    - Visitor
- **Patterns for Parallel Programming**
    - Task Decomposition
    - Divide and Conquer
    - Fork/Join
- **Patterns for Parallel Software Design**
    - *Communicating Sequential Elements*
    - *Manager - Workers*
    - *Shared Variable Channel*
    - *Semaphore*
- **Patterns for Concurrent and Networked Objects**
    - Active Object
- **Security Patterns - Integrating Security and Systems Engineering**
    - Access Control Requirements
    - Single Access Point
    - Check Point
    - Security Session
    - Full Access with Errors
    - Limited Access
    - Information Obscurity
    - Secure Channels
    - Known Partners
    - Demilitarized Zone
    - Protection Reverse Proxy
    - Integration Reverse Proxy
    - Front Door
- **Remoting Patterns - Foundations of Enterprise, Internet and Realtime Distributed Object Middleware**
    - Pooling
    - Lazy Acquisition
    - Leasing
    - Passivation
    - Invocation Interceptor
    - Poll Object vs. Result Callback
- **SOA Patterns**
    - *Workflodize*
    - Saga
