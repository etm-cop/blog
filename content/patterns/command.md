Title: Command Pattern
SubTitle: Pattern Showcase
Tags: design patterns, mentoring, presentations
Date: 2016-06-10
Author: Kevin Perry
Summary: Decouple the invocation of a function from the execution of an action
         by encapsulating all relevant information in a dedicated object.

The **Command Pattern** (aka **Action** or **Transaction**) belongs to the class
of *behavioral patterns*.
That means it is not only concerned with the individual objects and classes it
consists of but also - and probably more importantly - the communcation and
interaction between these objects.

It is intended for encapsulating individual and independent function
invocations, allowing them to be executed at a later point in time.
This enables loose coupling between entities invoking an action, the receiver
actually handling the request and anyone in between extending and passing around
encapsulated commands.

## Applications
The Command Pattern (and its variations) enable a wide range of applications.
Most commonly it is known for implementations of queueable and undoable
operations.
While probably the most common examples, text-formatting macros, request logging
and database transactions represent only a small subset of similar and more
varied applications.

## Structure
In its most common incarnation, the Command Pattern usually consists of
*clients*, *commands*, *invokers*, and *receivers*.

- A **client** instantiates and parametrizes individual **command** objects and
    passes them on to an **invoker**.
    Thusly, it expresses the *intent* of executing a specific request.
    It does not need to know *how* execution actually works or *when* and
    *where* the associated action takes place.
- A **command** object encapsulates all parameters for a given action.
    This object knows its **receiver** and *how* to execute the parametrized
    request on it.
    It does not care how it is created and by whom it is invoked, neither does
    it need to know the **receiver's** state.
    In certain incarnations the object also knows how to *undo* itself.
- An **invoker** takes **command** objects from a **client** and knows *how* and
    *when* to trigger their execution.
    It can be used to e.g. *queue*, *log*, or *undo* individual or related sets
    of **command** objects.
    The invoker does not care about the **receiver** or the parametrization of
    **command** objects.
- The **receiver** provides the actual functionality invoked by a **command**
    object.

![Command Pattern Participants]({filename}/images/patterns/classes_command.png)

## Related Patterns
- **Active Object** is a variation of the **Command** pattern with the express purpose of handling
    requests in a concurrent environment.
    The invoker usually is responsible for *scheduling* tasks and executing them in different thread
    contexts.
- The **Composite** pattern can be used to implement Macros.
- The **Decorator** pattern can be used to wrap commands for e.g. logging purposes.
- The **Memento** pattern can be used for keeping the state of more complex commands (e.g. for
    undoing operations).
- The **Flyweight** and **Prototype** patterns are useful when re-configuring or copying of commands
    is necessary.

### Bibliography
**Design Patterns: Elements of Reusable Object-Oriented Software**
:   *Erich Gamma*, *Richard Helm*, *Ralph Johnson*, *John Vlissides*
    Addison-Wesley, 1995

[**Command Pattern - Wikipedia, the free encyclopedia**](https://en.wikipedia.org/wiki/Command_pattern)

### Examples
- [C++14 Implementation Examples]({attach}/downloads/command_pattern.cxx)
- [patterns.pl Implementation Example](http://patterns.pl/command.html)
