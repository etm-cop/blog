#include <iostream>
#include <cstring>
#include <cstdlib>
#include <set>

class Foo
{
  public:
    Foo()
    {
      std::cout << "<< Expensive construction" << std::endl;
      _state = std::malloc(0xFFFF);
      std::memset(_state, 42, 0xFFFF);
    }

    Foo(Foo const & rhs)
    {
      std::cout << "<< Expensive copy construction" << std::endl;
      _state = std::malloc(0xFFFF);
      std::memcpy(_state, rhs._state, 0xFFFF);
    }

    ~Foo()
    {
      std::cout << "<< Expensive destruction" << std::endl;
      std::memset(_state, 0, 0xFFFF);
      std::free(_state);
    }

    bool operator<(Foo const & rhs) const { return _state < rhs._state; }

  private:
    void * _state = nullptr;
};

class Bar
{
  public:
    Bar()
    {
      std::cout << "<< Expensive construction" << std::endl;
      _state = std::malloc(0xFFFF);
      std::memset(_state, 42, 0xFFFF);
    }

    Bar(Bar const & rhs)
    {
      if ( nullptr == rhs._state )
      {
        std::cout << "<< Cheap copy construction" << std::endl;
        return;
      }

      std::cout << "<< Expensive copy construction" << std::endl;
      _state = std::malloc(0xFFFF);
      std::memcpy(_state, rhs._state, 0xFFFF);
    }

    Bar(Bar && rhs)
    {
      std::cout << "<< Cheap move construction" << std::endl;
      std::swap(_state, rhs._state);
    }

    ~Bar()
    {
      if ( nullptr == _state )
      {
        std::cout << "<< Cheap destruction" << std::endl;
        return;
      }

      std::cout << "<< Expensive destruction" << std::endl;
      std::memset(_state, 0, 0xFFFF);
      std::free(_state);
    }

    bool operator<(Bar const & rhs) const { return _state < rhs._state; }

  private:
    void * _state = nullptr;
};

int main(int, char **)
{
  std::cout << "class Foo" << std::endl;
  {
    std::cout << ">> Insert a local object" << std::endl;
    std::set<Foo> foos{};
    Foo f{};
    foos.insert(f);
  }

  std::cout << std::endl;

  {
    std::cout << ">> Insert a temporary" << std::endl;
    std::set<Foo> foos{};
    foos.insert(Foo{});
  }

  std::cout << std::endl;
  std::cout << "class Bar" << std::endl;

  {
    std::cout << ">> Insert a local object" << std::endl;
    std::set<Bar> bars{};
    Bar b{};
    bars.insert(std::move(b));
  }

  std::cout << std::endl;

  {
    std::cout << ">> Insert a temporary" << std::endl;
    std::set<Bar> bars{};
    bars.insert(Bar{});
  }

  return 0;
}
