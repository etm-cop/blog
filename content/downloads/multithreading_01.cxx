#include <chrono>
#include <future>
#include <iostream>
#include <thread>

using namespace std::literals;

int main()
{
  // the main() thread has its own ID
  auto const tid = std::this_thread::get_id();
  std::cout << "main() thread ID: " << tid << std::endl;

  // we're nice and give other threads the chance to be scheduled
  std::this_thread::yield();

  // we can go to sleep for given amounts of time
  auto const start = std::chrono::steady_clock::now();
  std::this_thread::sleep_for(500ms);
  std::this_thread::sleep_until(start + 1s);
  auto const stop = std::chrono::steady_clock::now();
  auto const waited = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
  std::cout << "waited for a total of " << waited.count() << "ms" << std::endl;

  // do we even support stuff running concurrently?
  auto const nof_threads = std::thread::hardware_concurrency();
  std::cout << nof_threads << " concurrent threads are supported" << std::endl;

  auto const printThreadId = []()
  {
    auto const tid = std::this_thread::get_id();

    // FIXME: we're not protecting std::cout against concurrent access
    std::cout << "my thread ID: " << tid << std::endl;
  };

  // just to check we actually start a new thread
  printThreadId();
  // creating a thread starts it immediately
  std::thread printer{printThreadId};
  // we wait for it to finish
  printer.join();

  auto const wait_for = [](std::chrono::seconds const dur)
  {
    std::this_thread::sleep_for(dur);
    std::cout << "I'm done after " << dur.count() << "s!" << std::endl;
  };

  std::thread waiter{wait_for, 1s};
  // we don't want to wait for it to finish
  waiter.detach();
  // but we make sure we outlive the thread
  wait_for(2s);

  auto const slow_add = [](unsigned const value1, unsigned const value2)
  {
    auto const tid = std::this_thread::get_id();
    std::cout << "Computing a sum in thread " << tid << "." << std::endl;

    std::this_thread::sleep_for(1s);
    return value1 + value2;
  };

  auto async_result = std::async(std::launch::async, slow_add, 2, 2);
  std::cout << "Result: " << async_result.get() << std::endl;

  return 0;
}
