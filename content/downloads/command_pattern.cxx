#include <chrono>
#include <future>
#include <iostream>
#include <memory>
#include <queue>
#include <string>
#include <thread>
#include <utility>

class ICommand
{
  public:
    virtual void execute() = 0;
};
using ICommandPtr = std::unique_ptr<ICommand>;

class PrintCommand final : public ICommand
{
  public:
    PrintCommand(std::string const & text)
      : text(text) { /* empty */ }

    void execute() override
    {
      std::cout << text << std::endl;
    }

  private:
    std::string const text;
};

class AbstractInvoker
{
  public:
    virtual void invoke(ICommandPtr command) { command->execute(); }
};

class TransactionInvoker final : AbstractInvoker
{
  public:
    void invoke(ICommandPtr command) override
    {
      commands.push(std::move(command));
    }

    void commit()
    {
      while ( !commands.empty() )
      {
        auto & command = commands.front();
        command->execute();
        commands.pop();
      }
    }

  private:
    std::queue<ICommandPtr> commands;
};

class AsyncInvoker final : AbstractInvoker
{
  public:
    ~AsyncInvoker()
    {
      while ( !futures.empty() )
      {
        auto & future = futures.front();
        future.wait();
        futures.pop();
      }
    }

    void invoke(ICommandPtr command, std::chrono::seconds delay)
    {
      auto future = std::async(std::launch::async,
          [command(std::move(command)), delay]
          {
            std::this_thread::sleep_for(delay);
            command->execute();
          });

      futures.push(std::move(future));
    }

  private:
    std::queue<std::future<void>> futures;
};

template<typename Function, typename... Args>
class ArbitraryCommand final : public ICommand
{
  public:
    ArbitraryCommand(Function && f, Args &&... args)
      : f{std::forward<Function>(f)}, args{std::forward<Args...>(args...)} { /* empty */ }

    void execute()
    {
      using Indices = std::index_sequence_for<Args...>;
      call(Indices{});
    }

  private:
    std::function<void(Args...)> f;
    std::tuple<Args...> args;

    template<size_t... I>
    void call(std::index_sequence<I...>)
    {
      f(std::get<I>(args)...);
    }
};

template<typename Function, typename... Args>
ArbitraryCommand<Function, Args...> make_command(Function && f, Args &&... args)
{
  return ArbitraryCommand<Function, Args...>{
    std::forward<Function>(f),
    std::forward<Args>(args)...};
}

int main(int, char **)
{
  {
    std::cout << "Execute commands immediately:" << std::endl;
    AbstractInvoker immediateInvoker;
    auto hwCommand = std::make_unique<PrintCommand>(" Hello, World!");
    immediateInvoker.invoke(std::move(hwCommand));
    std::cout << "---" << std::endl;
  }

  {
    std::cout << "Queue commands before execution:" << std::endl;
    TransactionInvoker transactionInvoker;
    auto hCommand = std::make_unique<PrintCommand>(" Hello,");
    auto wCommand = std::make_unique<PrintCommand>(" World!");
    transactionInvoker.invoke(std::move(hCommand));
    transactionInvoker.invoke(std::move(wCommand));
    transactionInvoker.commit();
    std::cout << "---" << std::endl;
  }

  {
    using namespace std::chrono_literals;
    std::cout << "Execute commands asynchronously:" << std::endl;

    {
      AsyncInvoker asyncInvoker;
      auto hCommand = std::make_unique<PrintCommand>(" Hello,");
      auto wCommand = std::make_unique<PrintCommand>(" World!");
      asyncInvoker.invoke(std::move(hCommand), 2s);
      asyncInvoker.invoke(std::move(wCommand), 1s);
    }

    std::cout << "---" << std::endl;
  }

  {
    std::cout << "Templated commands:" << std::endl;

    auto hwCommand = make_command(
        [](std::string const & text){ std::cout << text << std::endl; },
        std::string{" Hello, World!"});
    hwCommand.execute();

    class Adder
    {
      public:
        Adder(unsigned summand) : summand(summand) { /* empty */ }

        void operator()(unsigned & base) const { base += summand; }

      private:
        unsigned const summand;
    };

    unsigned value = 0;
    auto add4Command = make_command(Adder{4}, std::ref(value));
    std::cout << " Value before: " << value;
    add4Command.execute();
    std::cout << ", Value after: " << value << std::endl;

    std::cout << "---" << std::endl;
  }

  return 0;
}
