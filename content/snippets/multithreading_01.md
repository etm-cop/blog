Title: Tending to the Lock
SubTitle: STL Concurrency Primitives
Tags: STL, concurrency, multithreading, C++11
Date: 2016-06-26
Author: Kevin Perry
Summary: STL multithreading support introduced with C++11

> Some people, when confronted with a problem, think &ldquo;I know, I'll use multithreading&rdquo;.
> Nothhw tpe yawrve o oblems.

Along with all that nifty other stuff, C++11 finally introduced support for multithreading into the STL.
That means we no longer have to rely on OS-specific or third-party implementations.

While I certainly wouldn't want to encourage every software developer to go crazy with threads, it
certainly cannot hurt having a grasp of what the STL has in store.
In this article, I will strive to highlight the most important support constructs introduced with
C++11 (and later versions of the standard).

# `<thread>`
The `<thread>` header contains your basic `std::thread` class and functions to interact with the
current and/or other threads.
The `std::this_thread` namespace provides a few functions to interact with the current thread of
execution:

    :::C++
    // the main() thread has its own ID
    auto const tid = std::this_thread::get_id();
    std::cout << "main() thread ID: " << tid << std::endl;

    // we're nice and give other threads the chance to be scheduled
    std::this_thread::yield();

    // we can go to sleep for given amounts of time
    auto const start = std::chrono::steady_clock::now();
    std::this_thread::sleep_for(500ms);
    std::this_thread::sleep_until(start + 1s);
    auto const stop = std::chrono::steady_clock::now();
    auto const waited = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "waited for a total of " << waited.count() << "ms" << std::endl;

Actually starting a new thread is rather easy.
The `std::thread` constructor takes a `Callable` (which can be a function, Functor, lambda etc.) and
a set of arguments.
This callable is consequently invoked in a different thread context:

    :::C++
    // do we even support stuff running concurrently?
    auto const nof_threads = std::thread::hardware_concurrency();
    std::cout << nof_threads << " concurrent threads are supported" << std::endl;

    auto const printThreadId = []()
    {
      auto const tid = std::this_thread::get_id();

      // FIXME: we're not protecting std::cout against concurrent access
      std::cout << "my thread ID: " << tid << std::endl;
    };

    // just to check we actually start a new thread
    printThreadId();
    // creating a thread starts it immediately
    std::thread printer{printThreadId};
    // we wait for it to finish
    printer.join();

As you can see, the `join()` method blocks the current thread context until the given thread has
finished executing.
In case you want to have a thread continue to execute independently (and are reasonably certain it
will finish before the main thread does), you can use `detach()` to do exactly that:

    :::C++
    auto const wait_for = [](std::chrono::seconds const dur)
    {
      std::this_thread::sleep_for(dur);
      std::cout << "I'm done after " << dur.count() << "s!" << std::endl;
    };

    std::thread waiter{wait_for, 1s};
    // we don't want to wait for it to finish
    waiter.detach();
    // but we make sure we outlive the thread
    wait_for(2s);

# `<future>`
While creating a new `std::thread` is trivial enough, the `<future>` header provides functionality
that makes it even easier to start an asynchronous operation providing a specific result at an
unspecified point of time.
`std::async` allows you to start an asynchronous task and returns a `std::future` providing access
to the task's result:

    :::C++
    auto const slow_add = [](unsigned const value1, unsigned const value2)
    {
      auto const tid = std::this_thread::get_id();
      std::cout << "Computing a sum in thread " << tid << "." << std::endl;

      std::this_thread::sleep_for(1s);
      return value1 + value2;
    };

    auto result = std::async(std::launch::async, slow_add, 2, 2);
    std::cout << "Result: " << result.get() << std::endl;

`std::future::get()` will block and wait for the operation to conclude before returning the result.
If you don't want to block your thread, you can use `std::future::valid()` to check whether a result
is available.
If you're only interested in whether the operation has concluded, you can use `wait`, `wait_for`, or
`wait_until` to wait for a given amount of time or until the operation is actually done.

[Full Source]({attach}/downloads/multithreading_01.cxx)
