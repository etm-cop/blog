Title: Deeply Moved
SubTitle: Move Semantics 101
Tags: move semantics, C++11
Date: 2015-11-07
Author: Kevin Perry
Summary: More efficient management of expensive objects in STL containers

As you are well aware, creating and destroying objects in C++ can be a time and
resource consuming task.
In this example, `class Foo` will manage a sizeable chunk of memory,
performing a deep copy in its copy constructor:

    :::C++
    class Foo
    {
      public:
        Foo()
        {
          _state = malloc(0xFFFF);
          std::memset(_state, 42, 0xFFFF);
        }

        Foo(Foo const & rhs)
        {
          _state = malloc(0xFFFF);
          std::memcpy(_state, rhs._state, 0xFFFF);
        }

        ~Foo()
        {
          std::memset(_state, 0, 0xFFFF);
          free(_state);
        }

      private:
        void * _state = nullptr;
    };

When managing instances of this class in, let's say, a `std::set` it will become
obvious why this may have an impact on performance:

    :::C++
    {
      std::set<Foo> foos{};

      Foo f{};
      foos.insert(f);
      foos.insert(Foo{});
    }

Both inserting a local object or a temporary will cause unnecessary memory
operations.
This particular snippet will cause 4 `malloc`, 4 `free`, 6 `memset`, and 2
`memcpy` calls.

More often than not we no longer need those local objects and temporaries after
adding them to a container.
So why not avoid those expensive and superfluous memory operations in the first
place?
Meet `class Bar` which deals with these problems a bit more intelligently:

    :::C++
    class Bar
    {
      public:
        Bar()
        {
          _state = malloc(0xFFFF);
          std::memset(_state, 42, 0xFFFF);
        }

        Bar(Bar const & rhs)
        {
          if ( nullptr == rhs._state )
            return;

          _state = malloc(0xFFFF);
          std::memcpy(_state, rhs._state, 0xFFFF);
        }

        Bar(Bar && rhs)
        {
          std::swap(_state, rhs._state);
        }

        ~Bar()
        {
          if ( nullptr == _state )
            return;

          std::memset(_state, 0, 0xFFFF);
          free(_state);
        }

      private:
        void * _state = nullptr;
    };

While `class Bar` more or less does the same thing as `class Foo`, it adds
support for *move construction*.
Instead of creating a deep copy, the move constructor `Bar(Bar && rhs)` simply
grabs the state of another object, leaving behind an empty husk of an object.
Incidentally, these husks are cheap to destroy (and copy).

Looking at a `std::set` containing `Bar` instances, the benefits of moving state
(as compared to copying it) should become obvious:

    :::C++
    {
      std::set<Bar> bars{};

      Bar b{};
      bars.insert(std::move(b));
      bars.insert(Bar{});
    }

Compared to the previous example, this snippet will cause far fewer expensive
memory operations:
Only two objects are fully constructed and destroyed for a total of 2 `malloc`,
2 `free`, and 4 `memset` calls.
Note the use of [`std::move`](http://en.cppreference.com/w/cpp/utility/move)
which we use to indicate that the local object `b` may be moved from.
Without this indication another deep copy would have been performed.

[Full Source]({attach}/downloads/move_semantics_01.cxx)
