#!/usr/bin/env python3
# vim:fileencoding=utf-8:ts=8:et:sw=4:sts=4:tw=79

from os import getenv

AUTHOR = getenv('COP_AUTHOR')
SITENAME = 'Community of Professionals'
SITEURL = getenv('COP_SITEURL', '')
THEME = 'medius'

PATH = 'content'
STATIC_PATHS = [
        'downloads',
        'static/robots.txt',
        'static/favicon.ico',
        'images',
        ]
EXTRA_PATH_METADATA = {
        'static/robots.txt': {'path': 'robots.txt'},
        'static/favicon.ico': {'path': 'favicon.ico'},
        }
OUTPUT_PATH = getenv('COP_OUTPUT_PATH', 'output')

TIMEZONE = 'UTC'

DEFAULT_DATE_FORMAT = '%Y-%m-%d'
DEFAULT_LANG = 'en'
DEFAULT_PAGINATION = False

# don't generate any feeds
AUTHOR_FEED_ATOM = AUTHOR_FEED_RSS = None
CATEGORY_FEED_ATOM = CATEGORY_FEED_RSS = None
FEED_ALL_ATOM = FEED_ALL_RSS = None
FEED_ATOM = FEED_RSS = None
TAG_FEED_ATOM = TAG_FEED_RSS = None
TRANSLATION_FEED_ATOM = TRANSLATION_FEED_RSS = None
